//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ElementDataVerification.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class InvoiceVerification
    {
        public int ID { get; set; }
        public string Facility { get; set; }
        public string Invoice { get; set; }
        public string IsVerified { get; set; }
        public System.DateTime BOTVerificationDate { get; set; }
        public string ReviewedBy { get; set; }
        public string Comments { get; set; }
        public Nullable<bool> IsApproved { get; set; }
        public Nullable<System.DateTime> ApprovedDate { get; set; }
    }
}
