﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ElementDataVerification.Models;

namespace ElementDataVerification.Controllers
{
    [Authorize]
    public class InvoiceVerificationsController : Controller
    {
        private GuardiansRPAEntities db = new GuardiansRPAEntities();

        // GET: InvoiceVerifications
        public ActionResult Index(string searchString, bool? showApproved, bool? showOnlyApproved)
        {


            var invoiceVerifications = new List<InvoiceVerification>();

         




            return View(invoiceVerifications);
        }

        // GET: InvoiceVerifications/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            InvoiceVerification invoiceVerification = db.InvoiceVerifications.Find(id);
            if (invoiceVerification == null)
            {
                return HttpNotFound();
            }

            if (User.Identity.Name.ToUpper() != "ANALYST")
            {
                invoiceVerification.ReviewedBy = User.Identity.Name;
            }

            return View(invoiceVerification);
        }

        // GET: InvoiceVerifications/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: InvoiceVerifications/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Facility,Invoice,IsVerified,BOTVerificationDate,ReviewedBy,Comments,IsApproved,ApprovedDate")] InvoiceVerification invoiceVerification)
        {
            if (ModelState.IsValid)
            {
                db.InvoiceVerifications.Add(invoiceVerification);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(invoiceVerification);
        }

        // GET: InvoiceVerifications/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            InvoiceVerification invoiceVerification = db.InvoiceVerifications.Find(id);
            if (invoiceVerification == null)
            {
                return HttpNotFound();
            }
            return View(invoiceVerification);
        }

        // POST: InvoiceVerifications/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Facility,Invoice,IsVerified,BOTVerificationDate,ReviewedBy,Comments,IsApproved,ApprovedDate")] InvoiceVerification invoiceVerification)
        {
            if (ModelState.IsValid)
            {

                invoiceVerification.ApprovedDate = DateTime.Now;

                db.Entry(invoiceVerification).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(invoiceVerification);
        }

        // GET: InvoiceVerifications/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            InvoiceVerification invoiceVerification = db.InvoiceVerifications.Find(id);
            if (invoiceVerification == null)
            {
                return HttpNotFound();
            }
            return View(invoiceVerification);
        }

        // POST: InvoiceVerifications/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            InvoiceVerification invoiceVerification = db.InvoiceVerifications.Find(id);
            db.InvoiceVerifications.Remove(invoiceVerification);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
