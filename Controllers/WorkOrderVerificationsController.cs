﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ElementDataVerification.Models;

namespace ElementDataVerification.Controllers
{
    [Authorize]
    public class WorkOrderVerificationsController : Controller
    {
        private GuardiansRPAEntities db = new GuardiansRPAEntities();

        public ActionResult Index()
        {

            var workOrderVerifications = new List<WorkOrderVerification>();
            return View(workOrderVerifications);
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WorkOrderVerification workOrderVerification = db.WorkOrderVerifications.Find(id);
            if (workOrderVerification == null)
            {
                return HttpNotFound();
            }
            return View(workOrderVerification);
        }

        // GET: WorkOrderVerifications/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: WorkOrderVerifications/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Facility,Wrk,TwoCOCs,CoC,FinalReport,BOTVerificationDate,ReviewedBy,Comments,IsApproved,ApprovedDate")] WorkOrderVerification workOrderVerification)
        {
            if (ModelState.IsValid)
            {
                db.WorkOrderVerifications.Add(workOrderVerification);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(workOrderVerification);
        }

        // GET: WorkOrderVerifications/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WorkOrderVerification workOrderVerification = db.WorkOrderVerifications.Find(id);
            if (workOrderVerification == null)
            {
                return HttpNotFound();
            }

            if (User.Identity.Name.ToUpper() != "ANALYST")
            {
                workOrderVerification.ReviewedBy = User.Identity.Name;
            }

           

            return View(workOrderVerification);
        }

        // POST: WorkOrderVerifications/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Facility,Wrk,TwoCOCs,CoC,FinalReport,BOTVerificationDate,ReviewedBy,Comments,IsApproved,ApprovedDate")] WorkOrderVerification workOrderVerification)
        {
            if (ModelState.IsValid)
            {
                workOrderVerification.ApprovedDate = DateTime.Now;

                db.Entry(workOrderVerification).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(workOrderVerification);
        }

        // GET: WorkOrderVerifications/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WorkOrderVerification workOrderVerification = db.WorkOrderVerifications.Find(id);
            if (workOrderVerification == null)
            {
                return HttpNotFound();
            }
            return View(workOrderVerification);
        }

        // POST: WorkOrderVerifications/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            WorkOrderVerification workOrderVerification = db.WorkOrderVerifications.Find(id);
            db.WorkOrderVerifications.Remove(workOrderVerification);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
