﻿using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using ElementDataVerification.Models;

namespace ElementDataVerification.Controllers
{
    [Route("api/WorkOrderVerificationsGrid/{action}", Name = "WorkOrderVerificationsGridApi")]
    public class WorkOrderVerificationsGridController : ApiController
    {
        private GuardiansRPAEntities _context = new GuardiansRPAEntities();

        [HttpGet]
        public async Task<HttpResponseMessage> Get(DataSourceLoadOptions loadOptions) {
            var workorderverifications = _context.WorkOrderVerifications.Select(i => new {
                i.ID,
                i.Facility,
                i.Wrk,
                i.TwoCOCs,
                i.CoC,
                i.FinalReport,
                i.BOTVerificationDate,
                i.ReviewedBy,
                i.Comments,
                i.IsApproved,
                i.ApprovedDate
            }).Where(i => (i.CoC != "Pass" || i.FinalReport != "Pass")  );

            

            return Request.CreateResponse(await DataSourceLoader.LoadAsync(workorderverifications, loadOptions));
        }

        [HttpPost]
        public async Task<HttpResponseMessage> Post(FormDataCollection form) {
            var model = new WorkOrderVerification();
            var values = JsonConvert.DeserializeObject<IDictionary>(form.Get("values"));
            PopulateModel(model, values);

            Validate(model);
            if (!ModelState.IsValid)
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, GetFullErrorMessage(ModelState));

            var result = _context.WorkOrderVerifications.Add(model);
            await _context.SaveChangesAsync();

            return Request.CreateResponse(HttpStatusCode.Created, new { result.ID });
        }

        [HttpPut]
        public async Task<HttpResponseMessage> Put(FormDataCollection form) {
            var key = Convert.ToInt32(form.Get("key"));
            var model = await _context.WorkOrderVerifications.FirstOrDefaultAsync(item => item.ID == key);
            if(model == null)
                return Request.CreateResponse(HttpStatusCode.Conflict, "Object not found");

            var values = JsonConvert.DeserializeObject<IDictionary>(form.Get("values"));
            PopulateModel(model, values);

            Validate(model);
            if (!ModelState.IsValid)
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, GetFullErrorMessage(ModelState));

            await _context.SaveChangesAsync();

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpDelete]
        public async Task Delete(FormDataCollection form) {
            var key = Convert.ToInt32(form.Get("key"));
            var model = await _context.WorkOrderVerifications.FirstOrDefaultAsync(item => item.ID == key);

            _context.WorkOrderVerifications.Remove(model);
            await _context.SaveChangesAsync();
        }


        private void PopulateModel(WorkOrderVerification model, IDictionary values) {
            string ID = nameof(WorkOrderVerification.ID);
            string FACILITY = nameof(WorkOrderVerification.Facility);
            string WRK = nameof(WorkOrderVerification.Wrk);
            string TWO_COCS = nameof(WorkOrderVerification.TwoCOCs);
            string CO_C = nameof(WorkOrderVerification.CoC);
            string FINAL_REPORT = nameof(WorkOrderVerification.FinalReport);
            string BOTVERIFICATION_DATE = nameof(WorkOrderVerification.BOTVerificationDate);
            string REVIEWED_BY = nameof(WorkOrderVerification.ReviewedBy);
            string COMMENTS = nameof(WorkOrderVerification.Comments);
            string IS_APPROVED = nameof(WorkOrderVerification.IsApproved);
            string APPROVED_DATE = nameof(WorkOrderVerification.ApprovedDate);

            if(values.Contains(ID)) {
                model.ID = Convert.ToInt32(values[ID]);
            }

            if(values.Contains(FACILITY)) {
                model.Facility = Convert.ToString(values[FACILITY]);
            }

            if(values.Contains(WRK)) {
                model.Wrk = Convert.ToString(values[WRK]);
            }

            if(values.Contains(TWO_COCS)) {
                model.TwoCOCs = Convert.ToBoolean(values[TWO_COCS]);
            }

            if(values.Contains(CO_C)) {
                model.CoC = Convert.ToString(values[CO_C]);
            }

            if(values.Contains(FINAL_REPORT)) {
                model.FinalReport = Convert.ToString(values[FINAL_REPORT]);
            }

            if(values.Contains(BOTVERIFICATION_DATE)) {
                model.BOTVerificationDate = Convert.ToDateTime(values[BOTVERIFICATION_DATE]);
            }

            if(values.Contains(REVIEWED_BY)) {
                model.ReviewedBy = Convert.ToString(values[REVIEWED_BY]);
            }

            if(values.Contains(COMMENTS)) {
                model.Comments = Convert.ToString(values[COMMENTS]);
            }

            if(values.Contains(IS_APPROVED)) {
                model.IsApproved = values[IS_APPROVED] != null ? Convert.ToBoolean(values[IS_APPROVED]) : (bool?)null;
            }

            if(values.Contains(APPROVED_DATE)) {
                model.ApprovedDate = values[APPROVED_DATE] != null ? Convert.ToDateTime(values[APPROVED_DATE]) : (DateTime?)null;
            }
        }

        private string GetFullErrorMessage(ModelStateDictionary modelState) {
            var messages = new List<string>();

            foreach(var entry in modelState) {
                foreach(var error in entry.Value.Errors)
                    messages.Add(error.ErrorMessage);
            }

            return String.Join(" ", messages);
        }

        protected override void Dispose(bool disposing) {
            if (disposing) {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}