﻿using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using ElementDataVerification.Models;

namespace ElementDataVerification.Controllers
{
    [Route("api/InvoiceVerifications1/{action}", Name = "InvoiceVerifications1Api")]
    public class InvoiceVerifications1Controller : ApiController
    {
        private GuardiansRPAEntities _context = new GuardiansRPAEntities();

        [HttpGet]
        public async Task<HttpResponseMessage> Get(DataSourceLoadOptions loadOptions) {
            var invoiceVerifications = _context.InvoiceVerifications.Select(i => new {
                i.ID,
                i.Facility,
                i.Invoice,
                i.IsVerified,
                i.BOTVerificationDate,
                i.ReviewedBy,
                i.Comments,
                i.IsApproved,
                i.ApprovedDate
            }).Where(i => i.IsVerified != "Pass");

            // If you work with a large amount of data, consider specifying the PaginateViaPrimaryKey and PrimaryKey properties.
            // In this case, keys and data are loaded in separate queries. This can make the SQL execution plan more efficient.
            // Refer to the topic https://github.com/DevExpress/DevExtreme.AspNet.Data/issues/336.
            // loadOptions.PrimaryKey = new[] { "ID" };
            // loadOptions.PaginateViaPrimaryKey = true;

            return Request.CreateResponse(await DataSourceLoader.LoadAsync(invoiceVerifications, loadOptions));
        }

        [HttpPost]
        public async Task<HttpResponseMessage> Post(FormDataCollection form) {
            var model = new InvoiceVerification();
            var values = JsonConvert.DeserializeObject<IDictionary>(form.Get("values"));
            PopulateModel(model, values);

            Validate(model);
            if (!ModelState.IsValid)
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, GetFullErrorMessage(ModelState));

            var result = _context.InvoiceVerifications.Add(model);
            await _context.SaveChangesAsync();

            return Request.CreateResponse(HttpStatusCode.Created, new { result.ID });
        }

        [HttpPut]
        public async Task<HttpResponseMessage> Put(FormDataCollection form) {
            var key = Convert.ToInt32(form.Get("key"));
            var model = await _context.InvoiceVerifications.FirstOrDefaultAsync(item => item.ID == key);
            if(model == null)
                return Request.CreateResponse(HttpStatusCode.Conflict, "Object not found");

            var values = JsonConvert.DeserializeObject<IDictionary>(form.Get("values"));
            PopulateModel(model, values);

            Validate(model);
            if (!ModelState.IsValid)
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, GetFullErrorMessage(ModelState));

            await _context.SaveChangesAsync();

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpDelete]
        public async Task Delete(FormDataCollection form) {
            var key = Convert.ToInt32(form.Get("key"));
            var model = await _context.InvoiceVerifications.FirstOrDefaultAsync(item => item.ID == key);

            _context.InvoiceVerifications.Remove(model);
            await _context.SaveChangesAsync();
        }


        private void PopulateModel(InvoiceVerification model, IDictionary values) {
            string ID = nameof(InvoiceVerification.ID);
            string FACILITY = nameof(InvoiceVerification.Facility);
            string INVOICE = nameof(InvoiceVerification.Invoice);
            string IS_VERIFIED = nameof(InvoiceVerification.IsVerified);
            string BOTVERIFICATION_DATE = nameof(InvoiceVerification.BOTVerificationDate);
            string REVIEWED_BY = nameof(InvoiceVerification.ReviewedBy);
            string COMMENTS = nameof(InvoiceVerification.Comments);
            string IS_APPROVED = nameof(InvoiceVerification.IsApproved);
            string APPROVED_DATE = nameof(InvoiceVerification.ApprovedDate);

            if(values.Contains(ID)) {
                model.ID = Convert.ToInt32(values[ID]);
            }

            if(values.Contains(FACILITY)) {
                model.Facility = Convert.ToString(values[FACILITY]);
            }

            if(values.Contains(INVOICE)) {
                model.Invoice = Convert.ToString(values[INVOICE]);
            }

            if(values.Contains(IS_VERIFIED)) {
                model.IsVerified = Convert.ToString(values[IS_VERIFIED]);
            }

            if(values.Contains(BOTVERIFICATION_DATE)) {
                model.BOTVerificationDate = Convert.ToDateTime(values[BOTVERIFICATION_DATE]);
            }

            if(values.Contains(REVIEWED_BY)) {
                model.ReviewedBy = Convert.ToString(values[REVIEWED_BY]);
            }

            if(values.Contains(COMMENTS)) {
                model.Comments = Convert.ToString(values[COMMENTS]);
            }

            if(values.Contains(IS_APPROVED)) {
                model.IsApproved = values[IS_APPROVED] != null ? Convert.ToBoolean(values[IS_APPROVED]) : (bool?)null;
            }

            if(values.Contains(APPROVED_DATE)) {
                model.ApprovedDate = values[APPROVED_DATE] != null ? Convert.ToDateTime(values[APPROVED_DATE]) : (DateTime?)null;
            }
        }

        private string GetFullErrorMessage(ModelStateDictionary modelState) {
            var messages = new List<string>();

            foreach(var entry in modelState) {
                foreach(var error in entry.Value.Errors)
                    messages.Add(error.ErrorMessage);
            }

            return String.Join(" ", messages);
        }

        protected override void Dispose(bool disposing) {
            if (disposing) {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}