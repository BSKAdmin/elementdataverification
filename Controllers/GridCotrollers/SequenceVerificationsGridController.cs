﻿using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using ElementDataVerification.Models;

namespace ElementDataVerification.Controllers
{
    [Route("api/SequenceVerificationsGrid/{action}", Name = "SequenceVerificationsGridApi")]
    public class SequenceVerificationsGridController : ApiController
    {
        private GuardiansRPAEntities _context = new GuardiansRPAEntities();

        [HttpGet]
        public async Task<HttpResponseMessage> Get(DataSourceLoadOptions loadOptions) {
            var sequenceverifications = _context.SequenceVerifications.Select(i => new {
                i.ID,
                i.Facility,
                i.Sequence,
                i.FileExists,
                i.CorrectSequence,
                i.BOTVerificationDate,
                i.ReviewedBy,
                i.Comments,
                i.IsApproved,
                i.ApprovedDate
            }).Where(i => (i.FileExists != "Pass" || i.CorrectSequence != "Pass"));


            return Request.CreateResponse(await DataSourceLoader.LoadAsync(sequenceverifications, loadOptions));
        }

        [HttpPost]
        public async Task<HttpResponseMessage> Post(FormDataCollection form) {
            var model = new SequenceVerification();
            var values = JsonConvert.DeserializeObject<IDictionary>(form.Get("values"));
            PopulateModel(model, values);

            Validate(model);
            if (!ModelState.IsValid)
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, GetFullErrorMessage(ModelState));

            var result = _context.SequenceVerifications.Add(model);
            await _context.SaveChangesAsync();

            return Request.CreateResponse(HttpStatusCode.Created, new { result.ID });
        }

        [HttpPut]
        public async Task<HttpResponseMessage> Put(FormDataCollection form) {
            var key = Convert.ToInt32(form.Get("key"));
            var model = await _context.SequenceVerifications.FirstOrDefaultAsync(item => item.ID == key);
            if(model == null)
                return Request.CreateResponse(HttpStatusCode.Conflict, "Object not found");

            var values = JsonConvert.DeserializeObject<IDictionary>(form.Get("values"));
            PopulateModel(model, values);

            Validate(model);
            if (!ModelState.IsValid)
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, GetFullErrorMessage(ModelState));

            await _context.SaveChangesAsync();

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpDelete]
        public async Task Delete(FormDataCollection form) {
            var key = Convert.ToInt32(form.Get("key"));
            var model = await _context.SequenceVerifications.FirstOrDefaultAsync(item => item.ID == key);

            _context.SequenceVerifications.Remove(model);
            await _context.SaveChangesAsync();
        }


        private void PopulateModel(SequenceVerification model, IDictionary values) {
            string ID = nameof(SequenceVerification.ID);
            string FACILITY = nameof(SequenceVerification.Facility);
            string SEQUENCE = nameof(SequenceVerification.Sequence);
            string FILE_EXISTS = nameof(SequenceVerification.FileExists);
            string CORRECT_SEQUENCE = nameof(SequenceVerification.CorrectSequence);
            string BOTVERIFICATION_DATE = nameof(SequenceVerification.BOTVerificationDate);
            string REVIEWED_BY = nameof(SequenceVerification.ReviewedBy);
            string COMMENTS = nameof(SequenceVerification.Comments);
            string IS_APPROVED = nameof(SequenceVerification.IsApproved);
            string APPROVED_DATE = nameof(SequenceVerification.ApprovedDate);

            if(values.Contains(ID)) {
                model.ID = Convert.ToInt32(values[ID]);
            }

            if(values.Contains(FACILITY)) {
                model.Facility = Convert.ToString(values[FACILITY]);
            }

            if(values.Contains(SEQUENCE)) {
                model.Sequence = Convert.ToString(values[SEQUENCE]);
            }

            if(values.Contains(FILE_EXISTS)) {
                model.FileExists = Convert.ToString(values[FILE_EXISTS]);
            }

            if(values.Contains(CORRECT_SEQUENCE)) {
                model.CorrectSequence = Convert.ToString(values[CORRECT_SEQUENCE]);
            }

            if(values.Contains(BOTVERIFICATION_DATE)) {
                model.BOTVerificationDate = Convert.ToDateTime(values[BOTVERIFICATION_DATE]);
            }

            if(values.Contains(REVIEWED_BY)) {
                model.ReviewedBy = Convert.ToString(values[REVIEWED_BY]);
            }

            if(values.Contains(COMMENTS)) {
                model.Comments = Convert.ToString(values[COMMENTS]);
            }

            if(values.Contains(IS_APPROVED)) {
                model.IsApproved = values[IS_APPROVED] != null ? Convert.ToBoolean(values[IS_APPROVED]) : (bool?)null;
            }

            if(values.Contains(APPROVED_DATE)) {
                model.ApprovedDate = values[APPROVED_DATE] != null ? Convert.ToDateTime(values[APPROVED_DATE]) : (DateTime?)null;
            }
        }

        private string GetFullErrorMessage(ModelStateDictionary modelState) {
            var messages = new List<string>();

            foreach(var entry in modelState) {
                foreach(var error in entry.Value.Errors)
                    messages.Add(error.ErrorMessage);
            }

            return String.Join(" ", messages);
        }

        protected override void Dispose(bool disposing) {
            if (disposing) {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}