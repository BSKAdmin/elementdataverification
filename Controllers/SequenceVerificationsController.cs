﻿using ElementDataVerification.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace ElementDataVerification.Controllers
{
    [Authorize]
    public class SequenceVerificationsController : Controller
    {
        private GuardiansRPAEntities db = new GuardiansRPAEntities();

        // GET: SequenceVerifications
        public ActionResult Index()
        {
          

            var sequenceVerifications = new List<SequenceVerification>();

           


            return View(sequenceVerifications);
        }

        // GET: SequenceVerifications/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SequenceVerification sequenceVerification = db.SequenceVerifications.Find(id);
            if (sequenceVerification == null)
            {
                return HttpNotFound();
            }

            if (User.Identity.Name.ToUpper() != "ANALYST")
            {
                sequenceVerification.ReviewedBy = User.Identity.Name;
            }

            return View(sequenceVerification);
        }

        // GET: SequenceVerifications/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: SequenceVerifications/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Facility,Sequence,FileExists,CorrectSequence,BOTVerificationDate,ReviewedBy,Comments,IsApproved,ApprovedDate")] SequenceVerification sequenceVerification)
        {
            if (ModelState.IsValid)
            {
                db.SequenceVerifications.Add(sequenceVerification);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(sequenceVerification);
        }

        // GET: SequenceVerifications/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SequenceVerification sequenceVerification = db.SequenceVerifications.Find(id);
            if (sequenceVerification == null)
            {
                return HttpNotFound();
            }
            return View(sequenceVerification);
        }

        // POST: SequenceVerifications/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Facility,Sequence,FileExists,CorrectSequence,BOTVerificationDate,ReviewedBy,Comments,IsApproved,ApprovedDate")] SequenceVerification sequenceVerification)
        {
            if (ModelState.IsValid)
            {
                sequenceVerification.ApprovedDate = DateTime.Now;

                db.Entry(sequenceVerification).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(sequenceVerification);
        }

        // GET: SequenceVerifications/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SequenceVerification sequenceVerification = db.SequenceVerifications.Find(id);
            if (sequenceVerification == null)
            {
                return HttpNotFound();
            }
            return View(sequenceVerification);
        }

        // POST: SequenceVerifications/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SequenceVerification sequenceVerification = db.SequenceVerifications.Find(id);
            db.SequenceVerifications.Remove(sequenceVerification);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
